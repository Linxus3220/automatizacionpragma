package co.com.demoblaze.models;

public class ModelDemoblaze {

    private String srtUser;
    private String srtPassword;
    private String srtNombreEnBarra;
    private String strNombreUsuario;
    private String strPasswordIncorrecto;
    private String strUsernameIncorrecto;
    private String srtPais;
    private String srtCiudad;
    private String srtTarjeta;
    private String srtMes;
    private String srtYear;
    private String srtMensajeFinal;
    private String strMensajeAlert;

    public void setSrtMensajeFinal(String srtMensajeFinal) {
        this.srtMensajeFinal = srtMensajeFinal;
    }

    public String getStrMensajeAlert() {
        return strMensajeAlert;
    }

    public void setStrMensajeAlert(String strMensajeAlert) {
        this.strMensajeAlert = strMensajeAlert;
    }

    public String getSrtMensajeFinal() {
        return srtMensajeFinal;
    }
    public void setSrtMesajeFinal(String srtMensajeFinal) {
        this.srtMensajeFinal = srtMensajeFinal;
    }

    public String getSrtPais() {
        return srtPais;
    }

    public void setSrtPais(String srtPais) {
        this.srtPais = srtPais;
    }

    public String getSrtCiudad() {
        return srtCiudad;
    }

    public void setSrtCiudad(String srtCiudad) {
        this.srtCiudad = srtCiudad;
    }

    public String getSrtTarjeta() {
        return srtTarjeta;
    }

    public void setSrtTarjeta(String srtTarjeta) {
        this.srtTarjeta = srtTarjeta;
    }

    public String getSrtMes() {
        return srtMes;
    }

    public void setSrtMes(String srtMes) {
        this.srtMes = srtMes;
    }

    public String getSrtYear() {
        return srtYear;
    }

    public void setSrtYear(String srtYear) {
        this.srtYear = srtYear;
    }

    public String getStrPasswordIncorrecto() {
        return strPasswordIncorrecto;
    }

    public void setStrPasswordIncorrecto(String strPasswordIncorrecto) {
        this.strPasswordIncorrecto = strPasswordIncorrecto;
    }

    public String getStrUsernameIncorrecto() {
        return strUsernameIncorrecto;
    }

    public void setStrUsernameIncorrecto(String strUsernameIncorrecto) {
        this.strUsernameIncorrecto = strUsernameIncorrecto;
    }

    public String getStrNombreUsuario() {
        return strNombreUsuario;
    }
    public void setStrNombreUsuario(String strNombreUsuario) {
        this.strNombreUsuario = strNombreUsuario;
    }
    public String getSrtUser() {
        return srtUser;
    }
    public void setSrtUser(String srtUser) {
        this.srtUser = srtUser;
    }
    public String getSrtPassword() {
        return srtPassword;
    }
    public void setSrtPassword(String srtPassword) {
        this.srtPassword = srtPassword;
    }
    public String getSrtNombreEnBarra() {
        return srtNombreEnBarra;
    }
    public void setSrtNombreEnBarra(String srtNombreEnBarra) {
        this.srtNombreEnBarra = srtNombreEnBarra;
    }
}
