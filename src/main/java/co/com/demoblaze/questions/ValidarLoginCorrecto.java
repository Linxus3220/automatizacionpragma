package co.com.demoblaze.questions;

import co.com.demoblaze.models.ModelDemoblaze;
import co.com.demoblaze.userinterface.HomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class ValidarLoginCorrecto implements Question<Boolean> {

    private List<ModelDemoblaze> datos;

    public ValidarLoginCorrecto(List<ModelDemoblaze> datos) {
        this.datos = datos;
    }

    public static ValidarLoginCorrecto es(List<ModelDemoblaze> datos) {
        return new ValidarLoginCorrecto(datos);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String texto_mensaje = Text.of(HomePage.NOMBRE_USUARIO).viewedBy(actor).asString();
        return  datos.get(0).getStrNombreUsuario().equals(texto_mensaje);
    }
}
