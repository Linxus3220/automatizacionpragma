package co.com.demoblaze.questions;

import co.com.demoblaze.models.ModelDemoblaze;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;

public class ValidarAlert implements Question<Boolean> {

    private List<ModelDemoblaze> datos;

    public ValidarAlert(List<ModelDemoblaze> datos) {
        this.datos = datos;
    }

    public static ValidarAlert es(List<ModelDemoblaze> datos) {
        return new ValidarAlert(datos);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return  datos.get(0).getStrMensajeAlert().equals(actor.recall("TextAlert"));
    }
}


