package co.com.demoblaze.questions;

import co.com.demoblaze.models.ModelDemoblaze;
import co.com.demoblaze.userinterface.PaginaCarrito;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class Respuesta implements Question<Boolean> {

    private List<ModelDemoblaze> datos;

    public Respuesta(List<ModelDemoblaze> datos) {
        this.datos = datos;
    }

    public static Respuesta es(List<ModelDemoblaze> datos) {
        return new Respuesta(datos);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String texto_mensaje = Text.of(PaginaCarrito.LABEL_COMPRA).viewedBy(actor).asString();
        return  datos.get(0).getSrtMensajeFinal().equals(texto_mensaje);
    }
}
