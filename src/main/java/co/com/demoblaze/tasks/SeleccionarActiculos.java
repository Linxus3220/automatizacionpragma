package co.com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;


import java.util.concurrent.TimeUnit;

import static co.com.demoblaze.userinterface.HomePage.ARTICULO_UNO;
import static co.com.demoblaze.userinterface.HomePage.BOTON_AGREGAR_CARRO;

public class SeleccionarActiculos implements Task {
    public static SeleccionarActiculos enLaPagina() {
        return Tasks.instrumented(SeleccionarActiculos.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(ARTICULO_UNO),
                Click.on(BOTON_AGREGAR_CARRO),
                AcceptAlert.accept()
        );
        try{
            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
