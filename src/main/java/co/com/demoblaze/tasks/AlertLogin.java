package co.com.demoblaze.tasks;

import co.com.demoblaze.models.ModelDemoblaze;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static co.com.demoblaze.userinterface.ModalLogin.*;
import static co.com.demoblaze.userinterface.ModalLogin.BOTON_LOGIN;

public class AlertLogin implements Task {

    private List<ModelDemoblaze> datos;

    public AlertLogin(List<ModelDemoblaze> datos) {
        this.datos = datos;
    }

    public static AlertLogin enLaPagina(List<ModelDemoblaze> datos) {
        return Tasks.instrumented(AlertLogin.class,datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(SIGN_UP),
                Enter.theValue(datos.get(0).getSrtUser()).into(INPUT_USER),
                Enter.theValue(datos.get(0).getSrtPassword()).into(INPUT_PASSWORD),
                Click.on(BOTON_LOGIN),
                AcceptAlert.accept()
        );
        try{
            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
