package co.com.demoblaze.tasks;

import co.com.demoblaze.models.ModelDemoblaze;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static co.com.demoblaze.userinterface.ModalRegistro.*;

public class Registrarse implements Task {

    private List<ModelDemoblaze> datos;

    public Registrarse(List<ModelDemoblaze> datos) {
        this.datos = datos;
    }

    public static Registrarse enLaPagina(List<ModelDemoblaze> datos) {
        return Tasks.instrumented(Registrarse.class,datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(SIGN_UP),
                Enter.theValue(datos.get(0).getSrtUser()).into(INPUT_USER_REG),
                Enter.theValue(datos.get(0).getSrtPassword()).into(INPUT_PASSWORD_REG),
                Click.on(BOTON_REG),
                AcceptAlert.accept()
        );
        try{
            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
