package co.com.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;
import net.serenitybdd.screenplay.Task;

import java.util.concurrent.TimeUnit;

public class AcceptAlert implements Task {

    public static AcceptAlert accept() {
        return new AcceptAlert();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        WebDriver driver = BrowseTheWeb.as(actor).getDriver();
        try{
            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        String mesajeAlert = driver.switchTo().alert().getText();
        actor.remember("TextAlert",mesajeAlert);
        driver.switchTo().alert().accept();
    }
}

