package co.com.demoblaze.tasks;

import co.com.demoblaze.models.ModelDemoblaze;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static co.com.demoblaze.userinterface.PaginaCarrito.*;

public class Comprar implements Task {

    private List<ModelDemoblaze> datos;
    public Comprar(List<ModelDemoblaze> datos) {
        this.datos = datos;
    }

    public static Comprar enLaPagina(List<ModelDemoblaze> datos) {
        return Tasks.instrumented(Comprar.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BOTON_CARRO),
                Click.on(BOTON_COMPRAR),
                Enter.theValue(datos.get(0).getStrNombreUsuario()).into(INPUT_NOMBRE),
                Enter.theValue(datos.get(0).getSrtPais()).into(INPUT_PAIS),
                Enter.theValue(datos.get(0).getSrtCiudad()).into(INPUT_CIUDAD),
                Enter.theValue(datos.get(0).getSrtTarjeta()).into(INPUT_TARJETA),
                Enter.theValue(datos.get(0).getSrtMes()).into(INPUT_MES),
                Enter.theValue(datos.get(0).getSrtYear()).into(INPUT_YEAR),
                Click.on(BOTON_COMPRAR_FINAL)

        );
        try{
            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
