package co.com.demoblaze.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ModalLogin {

    public static final Target SIGN_UP = Target.the("Boton para Login").located(By.id("login2"));
    public static final Target INPUT_USER = Target.the("Usuario").located(By.id("loginusername"));
    public static final Target INPUT_PASSWORD = Target.the("Password").located(By.id("loginpassword"));
    public static final Target BOTON_LOGIN = Target.the("Boton Login").located(By.xpath("/html/body/div[3]/div/div/div[3]/button[2]"));
}
