package co.com.demoblaze.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ModalRegistro {
    public static final Target SIGN_UP = Target.the("Boton para registro").located(By.id("signin2"));
    public static final Target INPUT_USER_REG = Target.the("Usuario").located(By.id("sign-username"));
    public static final Target INPUT_PASSWORD_REG = Target.the("Password").located(By.id("sign-password"));
    public static final Target BOTON_REG = Target.the("Boton Registro").located(By.xpath("/html/body/div[2]/div/div/div[3]/button[2]"));
}
