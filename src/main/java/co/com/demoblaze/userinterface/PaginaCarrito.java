package co.com.demoblaze.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaCarrito {
    public static final Target BOTON_CARRO = Target.the("Boton carro").located(By.id("cartur"));
    public static final Target BOTON_COMPRAR = Target.the("Boton comprar").located(By.xpath("/html/body/div[6]/div/div[2]/button"));
    public static final Target INPUT_NOMBRE = Target.the("Nombre").located(By.id("name"));
    public static final Target INPUT_PAIS = Target.the("Pais").located(By.id("country"));
    public static final Target INPUT_CIUDAD = Target.the("Ciudad").located(By.id("city"));
    public static final Target INPUT_TARJETA = Target.the("Tarjeta").located(By.id("card"));
    public static final Target INPUT_MES = Target.the("Mes").located(By.id("month"));
    public static final Target INPUT_YEAR = Target.the("Year").located(By.id("year"));
    public static final Target BOTON_COMPRAR_FINAL = Target.the("Comprar").located(By.xpath("/html/body/div[3]/div/div/div[3]/button[2]"));

    public static final Target LABEL_COMPRA = Target.the("Mensaje compra").located(By.xpath("/html/body/div[10]/h2"));
}
