package co.com.demoblaze.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomePage {
    public static final Target NOMBRE_USUARIO = Target.the("Nombre del usuario").located(By.id("nameofuser"));
    public static final Target ARTICULO_UNO = Target.the("Celular").located(By.xpath("/html/body/div[5]/div/div[2]/div/div[1]/div/div/h4/a"));
    public static final Target BOTON_AGREGAR_CARRO = Target.the("Agregar al Carro").located(By.xpath("/html/body/div[5]/div/div[2]/div[2]/div/a"));

}
