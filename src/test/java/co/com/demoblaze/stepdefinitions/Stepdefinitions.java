package co.com.demoblaze.stepdefinitions;

import co.com.demoblaze.models.ModelDemoblaze;
import co.com.demoblaze.questions.ValidarAlert;
import co.com.demoblaze.questions.Respuesta;
import co.com.demoblaze.questions.ValidarLoginCorrecto;
import co.com.demoblaze.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class Stepdefinitions {

    @Before
    public void setOnStage(){
        setTheStage(new OnlineCast());
    }

    @Given("^Que el usuario ingresa a la pagina de Demoblaze$")
    public void queElUsuarioIngresaALaPaginaDeDemoblaze(){
        theActorCalled("Miguel").wasAbleTo(AbrirPagina.enLaPagina());
    }

    @When("^Que un usuario ingresa sus credenciales en la pagina de Demoblaze$")
    public void queUnUsuarioIngresaSusCredencialesEnLaPaginaDeDemoblaze(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().attemptsTo(Loguearse.enLaPagina(datos));
    }

    @When("^Que un usuario ingresa sus credenciales en la pagina de Demoblaze y sale un alert$")
    public void queUnUsuarioIngresaSusCredencialesEnLaPaginaDeDemoblazeYSaleUnAlert(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().attemptsTo(AlertLogin.enLaPagina(datos));
    }

    @Then("^se vizualiza el nombre del usuario en la barra superior$")
    public void seVizualizaElNombreDelUsuarioEnLaBarraSuperior(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarLoginCorrecto.es(datos)));
    }

    @Then("^no se puede loguear correctamente$")
    public void noSePuedeLoguearCorrectamente(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarAlert.es(datos)));
    }

    @Then("^realiza una compra$")
    public void realizaUnaCompra(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().attemptsTo(SeleccionarActiculos.enLaPagina());
        theActorInTheSpotlight().attemptsTo(Comprar.enLaPagina(datos));
    }

    @Then("^puede ver el mensaje de compra correcta$")
    public void puedeVerElMensajeDeCompraCorrecta(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(Respuesta.es(datos)));
    }
    @When("^Que un usuario quiere registrarse en Demoblaze$")
    public void queUnUsuarioQuiereRegistrarseEnDemoblaze(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().attemptsTo(Registrarse.enLaPagina(datos));
    }
    @Then("^visualiza el mensaje$")
    public void visualizaElMensaje(List<ModelDemoblaze> datos){
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarAlert.es(datos)));
    }
}
