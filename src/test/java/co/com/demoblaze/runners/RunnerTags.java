package co.com.demoblaze.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/demoblaze.feature",
        glue = "co.com.demoblaze.stepdefinitions",
        snippets = SnippetType.CAMELCASE )
public class RunnerTags {
}
