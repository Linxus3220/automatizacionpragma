#Autor: Miguel Alvarez
@PruebaDemoblazePragma

Feature: Prueba Demoblaze

  Background:
    Given Que el usuario ingresa a la pagina de Demoblaze

  @LoginCorrecto
    Scenario: Loguearse
      When Que un usuario ingresa sus credenciales en la pagina de Demoblaze
        |srtUser   |srtPassword |
        |Linxus3220|1234        |
      Then se vizualiza el nombre del usuario en la barra superior
        |strNombreUsuario|
        |Welcome Linxus3220|

  @Compra
  Scenario: Comprar en demoblaze
    When Que un usuario ingresa sus credenciales en la pagina de Demoblaze
      |srtUser   |srtPassword |
      |Linxus3220|1234        |
    Then realiza una compra
    |strNombreUsuario|srtPais |srtCiudad|srtTarjeta|srtMes|srtYear|
    |Linxus3220      |Colombia|Sincelejo|0001      |01    |23     |
    And puede ver el mensaje de compra correcta
      |srtMensajeFinal            |
      |Thank you for your purchase!|

  @LoginPasswordIncorrecto
  Scenario: Loguearse con password incorrecto
    When Que un usuario ingresa sus credenciales en la pagina de Demoblaze y sale un alert
      |srtUser   |srtPassword |
      |Linxus3220|123456      |
    Then no se puede loguear correctamente
      |strMensajeAlert|
      |Wrong password.|

  @LoginUsernameIncorrecto
  Scenario: Loguearse con username incorrecto
    When Que un usuario ingresa sus credenciales en la pagina de Demoblaze y sale un alert
      |srtUser   |srtPassword |
      |Linxus3221|123456      |
    Then no se puede loguear correctamente
      |strMensajeAlert|
      |User does not exist.|

  @RegistroUsuarioNuevo
  Scenario: Registrar un usuario nuevo
    When Que un usuario quiere registrarse en Demoblaze
      |srtUser    |srtPassword |
      |Pepito3222 |1234        |
    Then visualiza el mensaje
      |strMensajeAlert|
      |Sign up successful.|

  @RegistrarUsuarioExistente
  Scenario: Registrar usuario existente
    When Que un usuario quiere registrarse en Demoblaze
      |srtUser   |srtPassword |
      |Linxus3220|1234        |
    Then visualiza el mensaje
      |strMensajeAlert|
      |This user already exist.|
